<?php

$response = array();

function sendSms($phone_number, $message) {
    
    $otp_prefix = ':';

    $response_type = 'json';

    $route = "4";
    
    $postData = array(
        'receptor' => $phone_number,
        'message' => $message
    );

    $url = "https://api.kavenegar.com/v1/484C36347A33452B386E56422B347A494A38324D58673D3D/sms/send.json?receptor=09186092955&message=2323";

    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $postData
    ));


    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    $output = curl_exec($ch);

    if (curl_errno($ch)) {
        echo 'error:' . curl_error($ch);
    }

    curl_close($ch);
}
?>