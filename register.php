<?php 

require "init.php";
require "sms.php";

$first_name = $_POST["first_name"];
$last_name = $_POST["last_name"];
$license_plate = $_POST["license_plate"];
$car_model = $_POST["car_model"];
$phone_number = $_POST["phone_number"];
$license_type = $_POST["license_type"];
$gender = $_POST["gender"];
$personal_code = $_POST["personal_code"];

$sql_register = "select password from tbl_users where phone_number like '".$phone_number."';";

$response_register = array();

$result_register = mysqli_query($db_conn, $sql_register);

if(!$result_register) {
	$code = "reg_fail";
	$message = "Registeration failed : something went wrong!";
	
	array_push($response_register, array("code"=>$code, "message"=>$message));
	echo json_encode($response_register);
	die("Error : Error in serching phone_number");
}


if (mysqli_num_rows($result_register) <= 0) {
	$rand_password = rand(1000, 9999);
	$sql_insert = "insert into tbl_users (
		first_name,
		last_name,
		license_plate,
		car_model,
		phone_number,
		license_type,
		gender,
		password,
		personal_code,
		status) 
	values('".$first_name."', 
		'".$last_name."', 
		'".$license_plate."', 
		'".$car_model."', 
		'".$phone_number."',
		'".$license_type."',
		'".$gender."',
		'".$rand_password."',
		'".$personal_code."',
		'0'
	);";
	
	$result_insert = mysqli_query($db_conn, $sql_insert);

	if(! $result_insert) {
		array_push($response_register, array("code"=>"reg_failed", "message"=>"Error in inserting account."));
		echo json_encode($response_register);
		
		die("Error : Error in insert user info");
	}
	
	sendSms($phone_number, $rand_password);
	
	$code = "reg_success";
	$message = $rand_password;
	array_push($response_register, array("code"=>$code, "message"=>$message));
	echo json_encode($response_register);
	
} else {
	
	$row = mysqli_fetch_row($result_register);
	$found_password = $row[0];
	$code = "reg_found";
	
	array_push($response_register, array("code"=>$code, "message"=>$found_password));
	echo json_encode($response_register);
	
	die("Error : Error in insert user info");
}

mysqli_close($db_conn);

?>
