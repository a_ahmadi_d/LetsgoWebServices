<?php 

require "init.php";
require "sms.php";

$phone_number = $_POST["phone_number"];

$sql_login = "select password from tbl_users where phone_number like '".$phone_number."';";

$result_login = mysqli_query($db_conn, $sql_login);
$response_login = array();

if(! $result_login) {
	array_push($response_login, array("code"=>"login_failed", "message"=>"Error in finding phone number!"));
	echo json_encode($response_login);
	
	die("Error : Something went wrong!");
}

if(mysqli_num_rows($result_login) > 0) {
	$row = mysqli_fetch_row($result_login);
	
	$password = $row[0];

	sendSms($phone_number, $password);
	
	$code = "login_success";
	$message = "SMS sent";
	array_push($response_login, array("code"=>$code, "message"=>$message));
	
	echo json_encode($response_login);
} else {
	$code = "login_failed";
	$message = "Phone number not found!";
	array_push($response_login, array("code"=>$code, "message"=>$message));

	echo json_encode($response_login);
}

mysqli_close($db_conn);

?>
