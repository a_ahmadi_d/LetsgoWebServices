<?php 
require "init.php";

$phone_number = $_POST["phone_number"];

$sql_info = "select first_name, last_name, phone_number, avatar, license_plate, coin, personal_code from tbl_users where phone_number like '".$phone_number."';";

$result_info = mysqli_query($db_conn, $sql_info);

$response_info = array();

if(!$result_info) {
	$code = "profile_failed";
	$message = "Erro in looking for phone number";
	
	array_push($response_info, array("code"=>$code, "message"=>$message));
	echo json_encode($response_info);
	die("Error : Error in serching phone_number");
}

if(mysqli_num_rows($result_info) > 0) {
	$row = mysqli_fetch_row($result_info);
	
	$first_name = $row[0];
	$last_name = $row[1];
	$phone_number = $row[2];
	$avatar = "http://192.168.82.201/images/profile.png";
	$license_plate = $row[4];
	$coin = $row[5];
	$national_code = $row[6];
	
	$code = "profile_success";

	array_push($response_info, array("code"=>$code, 
									 "first_name"=>$first_name, 
									 "last_name"=>$last_name, 
									 "phone_number"=>$phone_number, 
									 "avatar"=>$avatar, 
									 "license_plate"=>$license_plate, 
									 "coin"=>$coin, 
									 "national_code"=>$national_code));
	
	echo json_encode($response_info);
} else {
	$code = "profile_failed";
	$message = "Phone number not found!";
	array_push($response_info, array("code"=>$code, "message"=>$message));

	echo json_encode($response_info);
}

mysqli_close($db_conn);

?>