<?php 

require "init.php";

$user_id = $_POST["user_id"];

$sql_user_info = "select user_id, 
				  first_name, 
				  last_name, 
				  license_plate, 
				  car_model, 
				  phone_number, 
				  license_type, 
				  gender, avatar 
				  from tbl_users where user_id like '".$user_id."';";

$result_user_info = mysqli_query($db_conn, $sql_user_info);
$response_user_info = array();

if(! $result_user_info) {
	die("Error : Something went wrong!");
}

if(mysqli_num_rows($result_user_info) > 0) {
	$row = mysqli_fetch_row($result_user_info);
	
	$first_name = $row[0];
	$last_name = $row[1];
	$license_plate = $row[2];
	$car_model = $row[3];
	$phone_number = $row[4];
	$license_type = $row[5];
	$gender = $row[6];
	$avatar = $row[7];

	array_push($response_user_info, array(
								"code" => "success",
								"first_name" => $first_name, 
								"last_name" => $last_name, 
								"license_plate" => $license_plate,
								"car_model" => $car_model,
								"phone_number" => $phone_number,
								"license_type" => $license_type,
								"gender" => $gender,
								"avatar" => $avatar));
	
	echo json_encode($response_user_info);
} else {
	$code = "not_found";
	$message = "User with ".$user_id." not found";
	
	array_push($response_user_info, array("code"=>$code, "message"=>$message));

	echo json_encode($response_user_info);
}

mysqli_close($db_conn);

?>
