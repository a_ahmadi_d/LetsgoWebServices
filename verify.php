<?php 

require "init.php";

$phone_number = $_POST["phone_number"];
$user_password = $_POST['user_password'];

$sql_check = "SELECT password FROM tbl_users WHERE phone_number = '".$phone_number."';";

$result_check = mysqli_query($db_conn, $sql_check);
$response_verify = array();

if(! $result_check) {
	array_push($response_verify, array("code"=>"verify_failed", "message"=>"Error in finding phone number."));
	echo json_encode($response_verify);
	
	die("Error : Something went wrong!");
}

if(mysqli_num_rows($result_check) > 0) {
	$row = mysqli_fetch_row($result_check);
	
	$password = $row[0];

	if(strcmp($password,  $user_password) == 0) {
		$sql_verify = "UPDATE tbl_users SET status = 1 WHERE phone_number ='".$phone_number."';";

		$result_verify = mysqli_query( $db_conn, $sql_verify);

		if (!$result_verify) {
			array_push($response_verify, array("code"=>"verify_failed", "message"=>"Error in updating status."));
			echo json_encode($response_verify);
	
			die("Error : Something went wrong");
		} else {
			array_push($response_verify, array("code"=>"verify_done", "message"=>"Your account has activated now"));
			echo json_encode($response_verify);	
		}
	}
	
} else {
	$code = "verify_failed";
	$message = "Password not match";
	array_push($response_login, array("code"=>$code, "message"=>$message));

	echo json_encode($response_login);
}

mysqli_close($db_conn);

?>